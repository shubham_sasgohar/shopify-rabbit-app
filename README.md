# Shopify Rabbit App

Shopify Rabbit App is a custom Shopify application built using Django for the backend and React with the Shopify Polaris framework for the frontend. The app is designed to enhance the functionality of Shopify stores by providing additional features such as order and product management.

## Features

- Display a list of orders from your Shopify store.
- Display a list of products from your Shopify store.
- Add more features as per your requirements.

## Installation

Before you can use the Shopify Rabbit App, you need to set up your development environment:

**Clone the repository:**
git clone https://gitlab.com/shubham_sasgohar/shopify-rabbit-app.git
cd shopify-rabbit-app

**Set up the backend (Django):**
Navigate to the backend directory:
cd backend
Install the required Python packages:
pip install -r requirements.txt
Set up the environment variables:
cp .env.example .env
Edit the `.env` file and fill in your Shopify app credentials and other settings.
Run the Django migrations:
Start the Django development server:


**Set up the frontend (React):**

Navigate to the frontend directory:

Install the required Node.js packages: npm install
Set up the environment variables:

Edit the `.env` file and fill in your API base URL.

Start the React development server: npm start

## Usage

After setting up the app, you can access the frontend at `http://localhost:3000` and the backend at `http://localhost:8000`. The app will display a list of orders and products from your Shopify store.

## Contributing

Contributions to the Shopify Rabbit App are welcome. Please follow the standard GitHub workflow for submitting pull requests.

## License

Shopify Rabbit App is released under the [MIT License](LICENSE).
