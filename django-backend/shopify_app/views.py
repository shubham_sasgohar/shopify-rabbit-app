import requests
from django.http import JsonResponse
from django.conf import settings

# Assuming you have set SHOPIFY_ACCESS_TOKEN in your settings after authentication
ACCESS_TOKEN = settings.SHOPIFY_ACCESS_TOKEN

def get_access_token(request):
    code = request.GET.get('code')
    payload = {
        'client_id': settings.SHOPIFY_APP_API_KEY,
        'client_secret': settings.SHOPIFY_APP_API_SECRET,
        'code': code
    }

    response = requests.post(f'https://{settings.SHOPIFY_STORE_URL}/admin/oauth/access_token', data=payload)
    response_data = response.json()
    access_token = response_data.get('access_token')

    # Here you should store the access_token in your database or session

    return JsonResponse({'access_token': access_token})

def get_products(request):
    # Here you should retrieve the access token from your database or session where it is stored
    headers = {'X-Shopify-Access-Token': ACCESS_TOKEN}
    response = requests.get(f'https://{settings.SHOPIFY_STORE_URL}/admin/api/{settings.SHOPIFY_API_VERSION}/products.json', headers=headers)
    products = response.json().get('products')

    return JsonResponse({'products': products})

def get_orders(request):
    # Here you should retrieve the access token from your database or session where it is stored
    headers = {'X-Shopify-Access-Token': ACCESS_TOKEN}
    response = requests.get(f'https://{settings.SHOPIFY_STORE_URL}/admin/api/{settings.SHOPIFY_API_VERSION}/orders.json', headers=headers)
    orders = response.json().get('orders')

    return JsonResponse({'orders': orders})
