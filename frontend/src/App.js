import React from 'react';
import '@shopify/polaris/build/esm/styles.css';
import { AppProvider } from '@shopify/polaris';
import OrderList from './components/Orders/OrderList';

function App() {
  return (
    <AppProvider>
      <OrderList />
    </AppProvider>
  );
}

export default App;
