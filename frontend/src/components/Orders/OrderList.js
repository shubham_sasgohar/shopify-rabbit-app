import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Page, Card, DataTable, SkeletonPage, SkeletonBodyText, Layout } from '@shopify/polaris';

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
console.log('API Base URL:', process.env.REACT_APP_API_BASE_URL);


function OrderList() {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get(`${API_BASE_URL}/api/orders/`)
      .then(response => {
        setOrders(response.data.orders);
      })
      .catch(error => {
        console.error('Error fetching orders:', error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const rows = orders.map((order) => [
    order.order_number,
    order.customer && order.customer.first_name + ' ' + order.customer.last_name,
    order.created_at,
    order.total_price,
    order.financial_status,
  ]);

  if (loading) {
    return (
      <SkeletonPage primaryAction>
        <Layout>
          <Layout.Section>
            <Card sectioned>
              <SkeletonBodyText />
            </Card>
          </Layout.Section>
        </Layout>
      </SkeletonPage>
    );
  }

  if (orders.length === 0) {
    return (
      <Page title="Orders">
        <Card sectioned>
          <p>No orders found.</p>
        </Card>
      </Page>
    );
  }

  return (
    <Page title="Orders">
      <Card>
        <DataTable
          columnContentTypes={[
            'text',  // for order numbers which are usually numeric
            'text',     // for customer names
            'text',     // for date created
            'numeric',  // for total price
            'text',     // for financial status
            'text',     // for fulfillment status
          ]}
          headings={[
            'Order Number',
            'Customer Name',
            'Date Created',
            'Total Price',
            'Payment Status',
          ]}
          rows={rows}
        />
      </Card>
    </Page>
  );
}

export default OrderList;
